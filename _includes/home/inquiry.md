## Anfrage

<p class="pb-1">
  Übersicht über die Belegung im Haus.
</p>

{% include calendar.html %}

<form action="send-inquery.php" method="post" name="anfrage">

  <h5>Kontaktinformationen</h5>
  <p>
    Um mit uns in Kontakt zu treten, füllen Sie bitte die unteren Felder aus.
    Wir werden uns dann schnellstmöglich mit Ihnen in Verbindung setzen.
  </p>
  <div class="form-group row">
    <div class="col-md-6 mb-3">
      <label for="form-first-name">Vorname</label>
      <input name="given-name" type="text" class="form-control" id="form-first-name" placeholder="Vorname" autocomplete="given-name" required>
    </div>
    <div class="col-md-6 mb-3">
      <label for="form-last-name">Nachname</label>
      <input name="family-name" type="text" class="form-control" id="form-last-name" placeholder="Nachname" autocomplete="family-name" required>
    </div>
  </div>

  <div class="form-group row">
    <div class="col-md-6 mb-3">
      <label for="form-street-address">Straße / Nr.</label>
      <input name="street-address" type="text" class="form-control" id="form-street-address" placeholder="Straße / Nr" autocomplete="address-line1">
    </div>
    <div class="col-md-3 mb-3">
      <label for="form-postal-code">Postleitzahl</label>
      <input name="postal-code" type="text" class="form-control" id="form-postal-code" placeholder="PLZ" autocomplete="postal-code">
    </div>
    <div class="col-md-3 mb-3">
      <label for="form-place">Ort</label>
      <input name="place" type="text" class="form-control" id="form-place" placeholder="Ort" autocomplete="address-level2">
    </div>
  </div>

  <div class="form-group row">
    <div class="col-md-4 mb-3">
      <label for="form-email">E-Mail</label>
      <input name="email" type="email" class="form-control" id="form-email" placeholder="E-Mail" autocomplete="email" required>
    </div>

    <div class="col-md-4 mb-3">
      <label for="form-telephone">Telefon</label>
      <input name="tel" type="tel" class="form-control" id="form-telephone" placeholder="Telefon" autocomplete="tel">
    </div>

    <div class="col-md-4 mb-3">
      <label for="form-fax">Fax</label>
      <input name="fax" type="tel" class="form-control" id="form-fax" placeholder="Fax" autocomplete="fax">
    </div>
  </div>

  <h5>Gew&uuml;nschter Zeitraum</h5>
  <div class="form-group row">
    <div class="col-md-4 mb-3">
      <label for="form-arrival">Anreise</label>
      <input name="arrival" type="date" class="form-control" id="form-arrival" data-date-format="yyyy-mm-dd" required>
    </div>
    <div class="col-md-4 mb-3">
      <label for="form-departure">Abreise</label>
      <input name="departure" type="date" class="form-control" id="form-departure" data-date-format="yyyy-mm-dd" required>
    </div>
    <div class="col-md-4 mb-3">
      <label for="form-persons">Anzahl Personen</label>
      <input name="persons" type="number" min="2" max="10" class="form-control" id="form-persons" value="2" required>
    </div>
  </div>

  <h5>Kommentar</h5>
  <div class="form-group">
    <textarea name="comment" class="form-control" id="form-comment" rows="3"></textarea>
  </div>
  <button type="submit" class="btn btn-primary has-spinner"><i class="spinner fas fa-sync-alt fa-spin"></i> Senden</button>
</form>
<div id="form-success" class="alert alert-success mt-2" style="display: none;" role="alert">
  Anfrage verschickt.
</div>
<div id="form-error" class="alert alert-danger mt-2" style="display: none;" role="alert">
  Error.
</div>
