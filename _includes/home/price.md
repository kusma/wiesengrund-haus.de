## Preise

Die Preise werden der Länge Ihres Aufenthaltes und der Personenzahl
angepasst. Vermietung erst ab 2 Nächten. Bei Langzeitmietung ist ein
Sonderpreis möglich. An Feiertagen (Ostern, Pfingsten etc.) können Sie
ausschließlich das ganze Haus für <span class="text-nowrap">100 €</span>
pro Nacht mieten.

<div id="card-deck" class="card-deck pb-3">
  {% for apartment in site.data.apartments %}
    {% include apartment-card.html apartment=apartment %}
  {% endfor %}
</div>

##### Zusätzliche Personen pro Nacht:

<table class="table table-striped table-sm">
  <tbody>
    <tr>
      <td>pro Erwachsener</td>
      <td class="text-right text-nowrap">15 €</td>
    </tr>
    <tr>
      <td>pro Kind <span class="text-nowrap">(bis 14 Jahren)</span></td>
      <td class="text-right text-nowrap">10 €</td>
    </tr>
  </tbody>
</table>

##### Zusätzliche Kosten:

<table class="table table-striped table-sm">
  <tbody>
    <tr>
      <td>Bettwäsche + Handtücher pro Person</td>
      <td class="text-right text-nowrap">8 €</td>
    </tr>
    <tr>
      <td>Tourismusabgabe <span class="text-nowrap">für Erwachsener</span></td>
      <td class="text-right text-nowrap">0,75 € / Tag</td>
    </tr>
    <tr>
      <td>Tourismusabgabe <span class="text-nowrap">für Kinder (ab 6 Jahren)</span></td>
      <td class="text-right text-nowrap">0,30 € / Tag</td>
    </tr>
  </tbody>
</table>
