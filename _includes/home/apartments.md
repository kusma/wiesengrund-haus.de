## Wohnungen

Wenn Sie die Ruhe und die Natur lieben, dann sind Sie in unserem Haus gut
beraten. Unser saniertes Fachwerkhaus liegt abseits der Straße auf einem
großen Grundstück. Unser Ferienhaus ist ab 2 Personen nutzbar und bietet
Platz für bis zu 8 Personen. Durch die 3 Bäder ist es bestens geeignet für
größere Familien und Treffen mit Freunden. Ein gemeinsamer Essplatz kann
genutzt werden. Im Außengelände können Sie ungestört verweilen und den
Abend am Grill oder am Feuer verbringen.

<div class="card-deck pb-3">
  {% for apartment in site.data.apartments %}
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">{{ apartment.title }}</h4>
      <div class="pb-2">
        {% include carousel.html id=apartment.carousel-id slides=apartment.images %}
      </div>
      <p class="card-text">{{ apartment.description }}</p>
    </div>
  </div>
  {% endfor %}
</div>

Das Haus ist mit Zentralheizung ausgestattet, an kühlen Tagen wird geheizt.
Im Erdgeschoss kann bei Bedarf auch der Kamin geheizt werden und im Bad gibt
es eine zusätzliche Fußbodenheizung. Auch in der kalten Jahreszeit wohnt man
hier gemütlich warm.
