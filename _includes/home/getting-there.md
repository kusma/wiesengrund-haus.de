## Anfahrt

##### Anreise mit Auto

Sie fahren über die A 17, Abfahrt Pirna, weiter auf B 172 in Richtung Bad Schandau (Hotellinie B).
In Pirna/OT Sonnenstein biegen Sie links nach Rathen/Struppen ab (Hotellinie G). In Thürmsdorf
nach Ortseingangsschild rechts in den Ort fahren unsere Einfahrt befindet sich auf der rechten
Straßenseite gegenüber eines Wiesengrundstücks. An der Einfahrt steht ein Sandstein mit der Nummer 17.

<div class="embed-responsive map card">
  <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2514.439210866781!2d14.030741316432733!3d50.934089979545135!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4709bb68deb8774d%3A0x6a15cb551f404802!2sHaus+Wiesengrund!5e0!3m2!1sen!2sin!4v1513770855560" allowfullscreen></iframe>
</div>

##### Anreise per Bahn

Fernzug bis Dresden oder Bad Schandau. S-Bahn bis Bad Königstein. Wir bieten
einen Abholservice. Bahnhof Königstein 45 Minuten, gute Busverbindung in
Thürmsdorf.
