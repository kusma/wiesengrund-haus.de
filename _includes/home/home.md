## Ferienwohnung Haus Wiesengrund

Unweit der Festung Königstein, am Fuße des Kleinen Bärensteins liegt der
Ferienort Thürmsdorf. Idealer Ausgangspunkt für Wanderungen über die imposanten
Berge, Besuche der Städte Pirna und Dresden und Fahrten in die Tschechische
Republik.

Abseits der Straße, ruhig und idyllisch liegt unser Hof. Romantisch fließt der
Dorfbach durch das Grundstück. Hier können Sie in Ruhe verweilen und sich von
den Unternehmungen des Tages erholen. Kinder sind willkommen, sie erobern das
Gelände gern. Gartenmöbel, Liegestühle, Grill und Feuerstelle stehen zur
Verfügung. Parkplätze befinden sich auf dem Grundstück.
