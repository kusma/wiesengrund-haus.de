<?php

function errorHandler($severity, $message, $file, $line)
{
  if (!(error_reporting() & $severity))
    return false;

  throw new ErrorException($message, 0, $severity, $file, $line);
}

set_error_handler("errorHandler");
ini_set('html_errors', false); // oh my god, PHP!

function emitResponse($response)
{
  header('Content-type: application/json');
  echo json_encode($response);
}

function getPostValue($key)
{
  if (!array_key_exists($key, $_POST))
    throw new Exception("Missing POST value \"$key\"");

  return $_POST[$key];
}

try
{
  $given_name = getPostValue('given-name');
  $family_name = getPostValue('family-name');
  $street_address = getPostValue('street-address');
  $postal_code = getPostValue('postal-code');
  $place = getPostValue('place');
  $email = getPostValue('email');
  $tel = getPostValue('tel');
  $fax = getPostValue('fax');
  $arrival = getPostValue('arrival');
  $departure = getPostValue('departure');
  $persons = getPostValue('persons');
  $comment = getPostValue('comment');

  $to = 'info@wiesengrund-haus.de';
  $subject = 'Anfrage über das Webformular';

  // reformat dates
  $arrival = date_format(new DateTime($arrival), "d.m.y");
  $departure = date_format(new DateTime($departure), "d.m.y");

  $message= "

  Adressangaben:
  $family_name, $given_name
  $street_address
  $postal_code $place
  Tel.: $tel
  Fax: $fax

  Angaben zum Aufenthalt:
  Anreise: $arrival
  Abreise: $departure
  Personenzahl: $persons

  Nachricht:
  $comment";

  $from = "$given_name $family_name <$email>";

  $headers = array(
    "From: info@wiesengrund-haus.de",
    "Reply-To: $from",
    "Content-Type: text/plain; charset=UTF-8",
    "Content-Transfer-Encoding: 8bit"
  );

  if (!mail($to, $subject, $message, implode("\r\n", $headers))) {
    throw new Exception(error_get_last()['message']);
  }

  emitResponse([ 'status' => 'success' ]);

}
catch (Exception $e)
{
  $response = [ 'status' => 'error', 'message' => $e->getMessage() ];
  emitResponse($response);
}
?>
